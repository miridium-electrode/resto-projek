import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:resto_projek/type.dart';
import 'package:http/http.dart' as http;

Future<List<Restaurant>> getRestoFromFile(BuildContext context) async {
  final assetBundle = DefaultAssetBundle.of(context);
  final data = await assetBundle.loadString('assets/resto.json');
  final body = jsonDecode(data);

  return body["restaurants"].map<Restaurant>(Restaurant.fromJson).toList();
}

Future<List<Restaurant>> getRestoFromHttp(String address) async {
  final res = await http.get(Uri.parse(address));

  if(res.statusCode == 200) {
    final b = jsonDecode(res.body);
    
    return b["restaurants"].map<Restaurant>(Restaurant.fromJson).toList();
  } else {
    throw Exception("fail to load restaurant");
  }
}