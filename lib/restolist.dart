import 'package:flutter/material.dart';
import 'package:resto_projek/getjson.dart';
import 'package:resto_projek/restodetail.dart';
import 'package:resto_projek/type.dart';

class RestoList extends StatelessWidget {
  const RestoList({Key? key}) : super(key: key);

  Widget restoBox(BuildContext context, Restaurant resto) {
    var descLeft = Expanded(
        child: Container(
      margin: const EdgeInsets.only(left: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Text(
            resto.name,
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          Row(children: <Widget>[
            const Icon(Icons.location_on),
            Text(resto.city),
          ]),
          Row(children: <Widget>[
            const Icon(Icons.star),
            Text(resto.rating.toString())
          ])
        ],
      ),
    ));

    return InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => RestoDetail(resto: resto)));
      },
      child: Container(
        margin: const EdgeInsets.all(20.0),
        height: 120,
        child: Row(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(16.0),
              child: Image.network(
                resto.pictureId,
                width: 180,
              ),
            ),
            descLeft
          ],
        ),
      ),
    );
  }

  Widget buildResto(List<Restaurant> restos) => CustomScrollView(
        physics: const BouncingScrollPhysics(),
        shrinkWrap: true,
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: Container(
              padding: const EdgeInsets.only(left: 20.0),
              height: 120,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: const <Widget>[
                  Text(
                    "Restaurant",
                    style: TextStyle(fontSize: 30),
                  ),
                  Text(
                    "Recommendation restauran for you",
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, i) {
                return restoBox(context, restos[i]);
              },
              childCount: restos.length,
            ),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder<List<Restaurant>>(
      future: getRestoFromHttp("http://10.0.2.2:8000"),
      builder: (context, snapshot) {
        final restos = snapshot.data;

        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return const Center(child: CircularProgressIndicator());
          default:
            if (snapshot.hasError) {
              return const Center(child: Text("some error occured"));
            } else {
              return buildResto(restos!);
            }
        }
      },
    ));
  }
}
