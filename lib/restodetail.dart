import 'package:flutter/material.dart';
import 'package:resto_projek/type.dart';

class RestoDetail extends StatelessWidget {
  final Restaurant resto;

  const RestoDetail({Key? key, required this.resto}): super(key: key);

  Widget titleDesc() => Container(
    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          resto.name,
          style: const TextStyle(
            fontSize: 24.0,
            fontWeight: FontWeight.bold
          ),
        ),
        Row(
          children: <Widget>[
            const Icon(Icons.location_on),
            Text(resto.city),
          ],
        ),
      ],
    ),
  );

  Widget headerDetail() => SliverToBoxAdapter(
    child: Container(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Image.network(
            resto.pictureId,
          ),
          titleDesc(),
          const Text(
            "Deskripsi",
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold
            ),
          ),
          Text(
            resto.description,
            overflow: TextOverflow.clip,
          ),
        ],
      ),
    ),
  );

  Widget foodList(BuildContext context) => SliverGrid(
    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 2,
      crossAxisSpacing: 10.0,
      mainAxisSpacing: 10.0,
    ),
    delegate: SliverChildBuilderDelegate((context, i) {
        return Container(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: Text(resto.menus.foods[i].name)
          )
        );
      },
      childCount: resto.menus.foods.length
    ),
  );

  Widget drinkList(BuildContext context) => SliverGrid(
    delegate: SliverChildBuilderDelegate((context, i) {
        return Container(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: Text(resto.menus.drinks[i].name)
          )
        );
      },
      childCount: resto.menus.drinks.length
    ),
    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 2,
    ),
  );

  Widget buildDetail(BuildContext context) => CustomScrollView(
    shrinkWrap: true,
    physics: const BouncingScrollPhysics(),
    slivers: <Widget>[
      headerDetail(),
      SliverToBoxAdapter(
        child: Container(
          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0),
          child: const Text(
            "Foods",
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold
            ),
          ),
        ),
      ),
      foodList(context),
      SliverToBoxAdapter(
        child: Container(
          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0),
          child: const Text(
            "Drinks",
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold
            ),
          ),
        ),
      ),
      drinkList(context),
    ],
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildDetail(context),
    );
  }
}