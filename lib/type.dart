class Food {
  String name;

  Food({required this.name});

  static Food fromJson(json) => Food(name: json['name']);
}

class Drink {
  String name;

  Drink({required this.name});

  static Drink fromJson(json) => Drink(name: json['name']);
}

class Menu {
  List<Food> foods;
  List<Drink> drinks;

  Menu({required this.foods, required this.drinks});

  static Menu fromJson(json) {
      var tf = <Food>[];
      var td = <Drink>[];

      if (json['foods'] != null) {
        json['foods'].forEach((v) {
          tf.add(Food.fromJson(v));
        });
      }

      if(json['drinks'] != null) {
        json['drinks'].forEach((v) {
          td.add(Drink.fromJson(v));
        });
      }

      return Menu(foods: tf, drinks: td);
  }
}

class Restaurant {
  String id;
  String name;
  String description;
  String pictureId;
  String city;
  num rating;
  Menu menus;

  Restaurant(
      {required this.id,
      required this.name,
      required this.description,
      required this.pictureId,
      required this.city,
      required this.rating,
      required this.menus});

  static Restaurant fromJson(json) {
      return Restaurant(
        id: json['id'],
        name: json['name'],
        description: json['description'],
        pictureId: json['pictureId'],
        city: json['city'],
        rating: json['rating'],
        menus: Menu.fromJson(json['menus']));
    }
}
